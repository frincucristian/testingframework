    package PageObjects;

    import org.openqa.selenium.By;
    import org.openqa.selenium.WebDriver;
    import org.openqa.selenium.WebElement;
    import org.openqa.selenium.interactions.Actions;
    import org.openqa.selenium.support.FindBy;
    import org.openqa.selenium.support.PageFactory;
    import org.openqa.selenium.support.ui.ExpectedConditions;
    import org.openqa.selenium.support.ui.WebDriverWait;

    public class AnyPage {

            private WebDriver driver;
            WebDriverWait wait;
            private String searchIt;

        public String getSearchIt() {
            return searchIt;
        }

        public void setSearchIt(String searchIt) {
            this.searchIt = searchIt;
        }

            @FindBy(xpath = "/html/body/div[2]/div[2]/div/table/tbody/tr/td[4]/div/login/div/div[2]")
            WebElement myAccount;

            @FindBy(id = "keyword")
            WebElement searchInput;

            @FindBy(xpath = "/html/body/div[2]/div[2]/div/table/tbody/tr/td[3]/div/form/button")
            WebElement submitButton;

            public AnyPage(WebDriver driver) {
                this.driver = driver;
                wait = new WebDriverWait(driver, 15);
                PageFactory.initElements(this.driver, this);
            }

        public AnyPage() {

        }

        public void search() {
                searchInput.clear();
                searchInput.sendKeys();
                submitButton.submit();
            }


            public void waitForAnyPage() {
                wait.until(ExpectedConditions.elementToBeClickable(submitButton));
            }
    }
