package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(id = "email_address")
    WebElement emailInput;

    @FindBy(xpath = "/html/body/div[3]/div[2]/div[4]/form/div[1]/div[2]/input")
    WebElement passwordInput;

    @FindBy(xpath = "/html/body/div[3]/div[2]/div[4]/form/div[1]/button")
    WebElement submitButton;

    @FindBy(xpath = "//*[@id=\"bodycode\"]/div[3]/div[2]")
    WebElement text;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void login(String email, String password) {
        emailInput.clear();
        emailInput.sendKeys(email);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        submitButton.submit();
    }

    public void openLoginPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/index.php?main_page=login");
        driver.get(hostname + "/index.php?main_page=login");
    }

    public void waitForLoginElement() {
        wait.until(ExpectedConditions.elementToBeClickable(submitButton));
    }

    public void hoverContulMeuLogin() {
        WebElement hoverButton = driver.findElement(By.xpath("//*[@id=\"login_header\"]/div"));
        Actions actions = new Actions(driver);
        actions.moveToElement(hoverButton).build().perform();
        WebElement itemMenuIn = driver.findElement(By.xpath("//*[@id=\"login_header\"]/div/div[3]/div"));
        System.out.println(itemMenuIn.getText() + " -> you are logged in");
    }

    public void hoverContulMeuLogout() {
        WebElement hoverButton = driver.findElement(By.xpath("//*[@id=\"login_header\"]/div"));
        Actions actions = new Actions(driver);
        actions.moveToElement(hoverButton).build().perform();
        WebElement itemMenuOut = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/table/tbody/tr/td[4]/div/login/div/div[3]/a[5]"));
        System.out.println(itemMenuOut.getText() + " -> click here to logout");
        itemMenuOut.click();
        System.out.println(text.getText() + " -> you are out");

    }

}
