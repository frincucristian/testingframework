package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FirstPage {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/table/tbody/tr/td[4]/div/login/div/div[2]")
    WebElement myAccount;

    @FindBy(id = "keyword")
    WebElement searchInput;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/table/tbody/tr/td[3]/div/form/button")
    WebElement submitButton;

    public FirstPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void search() {
        searchInput.clear();
        searchInput.sendKeys();
        submitButton.submit();
    }

    public void openFirstPage(String hostname) {
        System.out.println("Open the next url:" + hostname);
        driver.get(hostname);
    }


    public void waitForLoginPage() {
        wait.until(ExpectedConditions.elementToBeClickable(submitButton));
    }



}
