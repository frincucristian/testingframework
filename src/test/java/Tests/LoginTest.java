package Tests;

import Models.AccountModel;
import Models.LoginModel;
import PageObjects.AnyPage;
import PageObjects.LoginPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static Utils.OtherUtils.sanitizeNullDbString;

public class LoginTest extends Base {

    @DataProvider(name = "xmlDp")
    public Iterator<Object[]> xmlDpCollection() {
        Collection<Object[]> dp = new ArrayList<>();
        File f = new File("src\\test\\resources\\data\\testdata.xml");
        LoginModel lm = unMarshallLoginModel(f);
        dp.add(new Object[]{lm});
        return dp.iterator();
    }

    private LoginModel unMarshallLoginModel(File f) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(LoginModel.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (LoginModel) jaxbUnmarshaller.unmarshal(f);
        } catch (JAXBException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private void printData(LoginModel lm) {
        System.out.println(lm.getAccount().getEmail());
        System.out.println(lm.getAccount().getPassword());
        //System.out.println(lm.getGeneralError());
    }

    private void loginActions(LoginModel lm) {
        LoginPage lp = new LoginPage(driver);
        lp.openLoginPage(hostname);
        lp.waitForLoginElement();
        lp.login(lm.getAccount().getEmail(), lm.getAccount().getPassword());
        lp.hoverContulMeuLogin();

    }

    private void logoutActions() {
        LoginPage lo = new LoginPage(driver);
        lo.hoverContulMeuLogout();
    }


    @Test(dataProvider = "xmlDp")
    public void login(LoginModel lm) {
        printData(lm);
        loginActions(lm);
    }

    @Test
    public void logout() {
        logoutActions();
    }



}
