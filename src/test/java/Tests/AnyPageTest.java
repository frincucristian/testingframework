package Tests;

import Models.AccountModel;
import Models.LoginModel;
import PageObjects.AnyPage;
import PageObjects.LoginPage;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static Utils.OtherUtils.sanitizeNullDbString;

public class AnyPageTest extends Base {

    @DataProvider(name = "sqlDp")
    public Iterator<Object[]> sqlDpCollection() {
        Collection<Object[]> dp = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://" + dbHostname + ":" + dbPort + "/" + dbSchema,
                    dbUsername, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM domo.products;");
            while (results.next()) {
                AnyPage ap = new AnyPage();
                ap.setSearchIt(sanitizeNullDbString(results.getString("product")));

            }
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp.iterator();
    }

    private void SearchProducts (){
        openPage lp = new openPage(driver);
        lp.openPage(hostname);
        lp.
    }

    @Test(dataProvider = "sqlDp")
    public void sqlTest(LoginModel lm) {
        printData();
        loginActions(lm);
    }

}
