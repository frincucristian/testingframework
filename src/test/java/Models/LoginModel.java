package Models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoginModel {
        private AccountModel account;
        private String generalError;


        public AccountModel getAccount() {
            return account;
        }

        @XmlElement
        public void setAccount(AccountModel account) {
            this.account = account;
        }


        public String getGeneralError() {
            return generalError;
        }

        @XmlElement
        public void setGeneralError(String generalError) {
            this.generalError = generalError;
        }

//        public LoginModel(AccountModel account, String generalError) {
//            this.account = account;
//            this.generalError = generalError;
//        }

        public LoginModel() {

        }

//        public LoginModel(String email, String password, String generalError) {
//            AccountModel ac = new  AccountModel();
//            ac.setEmail(email);
//            ac.setPassword(password);
//            this.account = ac;
//            this.generalError = generalError;
//
//        }

}